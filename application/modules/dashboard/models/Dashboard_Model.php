<?php 
   Class Dashboard_Model extends CI_Model { 
    
      Public function __construct() { 
         parent::__construct();
         $this->core_Db=config_item('core_db');
 
        } 
        
    public function getNews() {
        return $this->db->select('n.id,heading,date,CONCAT(first_name," ", middle_name) As full_name')
                        ->from("$this->core_Db.news As n")  
                        ->join("$this->core_Db.employee_data As emp",'emp.employee_id = n.post_by')
                        ->get()->result_array();                 
    }    

    public function getNewsDetail($id) {
        return $this->db->select('heading,date,news_content,CONCAT(first_name," ", middle_name) As full_name')
                        ->from("$this->core_Db.news n")
                        ->where(['id'=>$id])  
                        ->join("$this->core_Db.employee_data emp",'emp.employee_id = n.post_by')
                        ->get()->row();                 
    }  

    public function getLinks() {
        $url= config_item('core_system') . config_item('gallery_url');
        return $this->db->select("if(url is not null and url!='',url,if(path is not null,concat('$url/',path),'')) as url,description as title")
                    ->where('gallery = ',0)
                    ->get("$this->core_Db.documents")
                    ->result_array();                                 
    }  

    public function getPhotos() {
        $url= config_item('core_system') . config_item('gallery_url');
        return $this->db->select("if(url,url,if(path is not null,concat('$url/',path),'')) as url,description as desc")
                        ->get_where("$this->core_Db.documents", ['gallery'=>'Yes'])
                        ->result_array();                                 
    }

    public function getBalance($emp_id) {
        return $this->db->select('annual_leave_balance As Annual Leave,sick_leave_balance As Sick Leave,court_leave_balance As Court Leave,
                                 maternity_leave_balance As Maternity Leave,paternity_leave_balance As Paternity Leave,compassionate_leave_balance As Compassionate Leave,
                                 marriage_leave_balance As Marriage Leave,educational_leave_balance As Educational Leave,medical_balance As Medical Refund')                               
                        ->from('tbl_users')
                        ->where(array('tbl_users.emp_id'=>$emp_id))
                        ->join('tbl_families', 'tbl_families.emp_id = tbl_users.emp_id AND relationship = "Self"')
                        ->get()->row();
       
    } 
    
  } 


  
 