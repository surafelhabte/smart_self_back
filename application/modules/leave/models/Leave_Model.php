<?php
   class Leave_Model extends CI_Model
   {
       public function __construct()
       {
           parent::__construct();
           $this->load->helper('uuid_gen');
           $this->core_Db=config_item('core_db');
       }

       private function already_request($employee_id, $start_date, $end_date)
       {
           $result = $this->db->from('tbl_leave as tl')
                            ->join('tbl_request as tr', 'tl.request_id = tr.id')
                            ->group_start()
                                ->group_start()
                                    ->where(['`from`>=' => $start_date , '`from`<=' => $end_date])
                                ->group_end()
                                ->group_start()
                                    ->or_where(['DATE_ADD(`from` , interval number_of_days Day)>=' => $start_date ,
                                            'DATE_ADD(`from` , interval number_of_days Day)<=' => $end_date])
                                ->group_end()
                            ->group_end()
                            ->where('requested_by', $employee_id)
                            ->count_all_results();

           return $result > 0;
       }
        
       public function create($post)
       {
           $data = json_decode($post['data'], true);
           
           $interval = $data['number_of_days'];
           $from = Date('Y-m-d', strtotime($data['from']));
           $data['from']=$from;
           $interval = "$from + $interval Day";
           $end_date=date('Y-m-d', strtotime($interval));
           $request = $this->already_request($post['emp_id'], $from, $end_date);
           if ($request) {
               return ['status'=>false,'message'=>'You Already Request Leave Before !'];
           }
           $rule=['field' => 'data','label' => 'data','rules' => 'required'];
           $this->form_validation->set_rules(array_merge($this->validation_request(), [$rule]))
                              ->set_data($post);
           unset($post['data']);
           if ($this->form_validation->run() === true) {
               if (request_status($post['action_id'], $post['emp_id'])) {
                   $post['id'] = uuid_gen();

                   $data['request_id'] = $post['id'];
                   $data['id'] = uuid_gen();
        
                   $this->form_validation->reset_validation()->set_rules($this->validation_body())->set_data($data);
                   if ($this->form_validation->run()) {
                       $this->db->trans_begin();
                       $this->db->insert('tbl_request', $post);
                       $this->db->insert('tbl_leave', $data);

                       if ($this->db->trans_status() === true) {
                           $this->db->trans_commit();
                           return ['status'=>true,'message'=>'request created successfully.'];
                       } else {
                           $this->db->trans_rollback();
                           return ['status'=>false,'message'=>'unable to create request.'];
                       }
                   } else {
                       return $this->form_validation->error_array();
                   }
               } else {
                   return ['status'=>false,'message'=>'You can not send this request at this stage while your previous request is inprocess !'];
               }
           } else {
               return $this->form_validation->error_array();
           }
       }

       public function update($post)
       {
           $this->form_validation->set_rules([['field' => 'data','label' => 'data','rules' => 'required']])->set_data($post);

           if ($this->form_validation->run()) {
               $data = json_decode($post['data'], true);
               $this->form_validation->reset_validation()
                                ->set_rules(array_merge($this->validation_body(), [['field' => 'id','label' => 'id','rules' => 'required']]))
                                ->set_data($data);

               if ($this->form_validation->run()) {
                   unset($data['requested_by']);
                   $this->db->trans_begin();
                   $this->db->update('tbl_leave', $data, ['id' => $data['id']]);

                   if ($this->db->trans_status() === true) {
                       $this->db->trans_commit();
                       return ['status'=>true,'message'=>'request updated successfully.'];
                   } else {
                       $this->db->trans_rollback();
                       return ['status'=>false,'message'=>'unable to update request.'];
                   }
               } else {
                   return $this->form_validation->error_array();
               }
           } else {
               return $this->form_validation->error_array();
           }
       }

       public function filter($post)
       {
           $this->form_validation->set_rules($this->validation_request())->set_data($post);

           if ($this->form_validation->run()) {
               return $this->db->select('req.id,name,IfNull(status,"Inprocess") as status,leave_name as type,step,DateDiff(DATE_ADD(`from`, INTERVAL `number_of_days` DAY),`from`) As total,req.created_date')
                        ->from('tbl_request as req')
                        ->where(['action_id'=>$post['action_id'],'emp_id'=>$post['emp_id']])
                        ->join('tbl_actions as ac', 'ac.id = req.action_id')
                        ->join('tbl_leave as lv', 'lv.request_id = req.id')
                        ->join("$this->core_Db.leave_definition as ld", 'ld.id = lv.type')

                        ->order_by('req.created_date', 'DESC')
                        ->get()->result_array();
           } else {
               return $this->form_validation->error_array();
           }
       }

       public function get($id)
       {
           return $this->db->select('lv.id,from,number_of_days,leave_name as type,comment,step,requested_by,is_loan')
                      ->from('tbl_request as req')
                      ->where(['req.id'=>$id])
                      ->join('tbl_leave as lv', 'lv.request_id = req.id')
                      ->join("$this->core_Db.leave_definition as ld", 'ld.id = lv.type')
                      ->limit(1)
                      ->get()->row();
       }

       public function delete($id)
       {
           if ($this->db->where(['id'=>$id, 'status'=>null])->from('tbl_request')->count_all_results() > 0) {
               $this->db->trans_begin();
               $this->db->delete('tbl_request', ['id' => $id]);
  
               if ($this->db->trans_status() === true) {
                   $this->db->trans_commit();
                   return ['status'=>true, 'message' =>'Request deleted successfully.'];
               } else {
                   $this->db->trans_rollback();
                   return ['status'=>false, 'message' =>'unable to delete request.'];
               }
           } else {
               return ['status'=>false, 'message' =>'the request is archived. you can not delete at this stage.'];
           }
       }

       private function validation_body()
       {
           return [
            ['field' => 'from','label' => 'from','rules' => 'required'],
            ['field' => 'number_of_days','label' => 'number of days','rules' => 'required'],
            ['field' => 'type','label' => 'type','rules' => 'required']
        ];
       }

       private function validation_request()
       {
           return [['field' => 'action_id','label' => 'action','rules' => 'required'],
                ['field' => 'emp_id','label' => 'employee id','rules' => 'required']
               ];
       }

       public function getBalance($post)
       {
           return $this->db->select($post['type']. ' As balance')->get_where('tbl_users', ['emp_id'=>$post['emp_id']])->row();
       }

       public function getLeaveType($post)
       {
           $emp_id = $post['emp_id'];
           $select = "ld.id,leave_name,per_year,if(lc.leave_id is null,
                        if(per_year=0,null,per_year - ifNull(leave_taken,0)),
                        lc.balance)  as balance,
                        carry_over,ifNull(leave_taken,0) as leave_taken";
           return $this->db->select($select, false)
                ->from("$this->core_Db.leave_definition As ld")
                ->join("$this->core_Db.employee_data As ed", "gender=sex or gender is null or gender=''")
                ->join(
                    "(select employee_id,sum(balance) as balance,leave_id from $this->core_Db.leave_calculation 
                        where expired=0 group by employee_id,leave_id) as lc",
                    "ld.id=lc.leave_id and lc.employee_id=ed.employee_id",
                    "left"
                )
                ->join("(select employee_id,ifNull(sum(IfNull(leave_taken,0)),0) leave_taken,leave_definition_id
                        from $this->core_Db.leave_grant where leave_to >= concat_ws('-',year(current_date),1,1) and leave_to <= concat_ws('-',year(current_date),12,31)
                        group by employee_id,leave_definition_id) as lg", "ld.id=lg.leave_definition_id and ed.employee_id=lg.employee_id where ed.employee_id='$emp_id'", "left")
                ->get()->result_array();
       }

       //schedules
       public function create_schedule($post)
       {
           $this->form_validation->set_rules([['field' => 'data','label' => 'data','rules' => 'required']])
                              ->set_data($post);

           if ($this->form_validation->run() === true) {
               $post = json_decode($post['data'], true);

               $this->db->trans_begin();
               $this->db->insert_batch("$this->core_Db.leave_plan", $post['schedules']);

               if ($this->db->trans_status() === true) {
                   $this->db->trans_commit();
                   return ['status'=>true,'message'=>'leave schedule created successfully.'];
               } else {
                   $this->db->trans_rollback();
                   return ['status'=>false,'message'=>'unable to create leave schedule.'];
               }
           } else {
               return $this->form_validation->error_array();
           }
       }

       public function update_schedule($post)
       {
           $this->form_validation->set_rules([['field' => 'data','label' => 'data','rules' => 'required']])
                              ->set_data($post);

           if ($this->form_validation->run() === true) {
               $post = json_decode($post['data'], true);
               $post = $post['schedules'][0];


               $this->db->trans_begin();
               $this->db->update("$this->core_Db.leave_plan", $post, ['id' => $post['id']]);

               if ($this->db->trans_status() === true) {
                   $this->db->trans_commit();
                   return ['status'=>true,'message'=>'leave schedule updated successfully.'];
               } else {
                   $this->db->trans_rollback();
                   return ['status'=>false,'message'=>'unable to update leave schedule.'];
               }
           } else {
               return $this->form_validation->error_array();
           }
       }

       public function schedules($post)
       {
           return $this->db->select('*')
                        ->get_where("$this->core_Db.leave_plan", ["employee_id"=>$post['employee_id']])
                        ->result_array();
       }

       public function schedule($id)
       {
           return $this->db->select('*')->get_where("$this->core_Db.leave_plan", ['id'=>$id])->result_array();
       }

       public function delete_schedule($id)
       {
        
        //check something befor you delete .

           $this->db->trans_begin();
           $this->db->delete("$this->core_Db.leave_plan", ['id' => $id]);
  
           if ($this->db->trans_status() === true) {
               $this->db->trans_commit();
               return ['status'=>true, 'message' =>'leave schedule deleted successfully.'];
           } else {
               $this->db->trans_rollback();
               return ['status'=>false, 'message' =>'unable to delete schedule.'];
           }
       }

       public function actual_balance($post)
       {
           $total = $this->db->select('max(year) as year, sum(balance) as balance')
                            ->from("$this->core_Db.leave_calculation")
                            ->where(['employee_id'=>$post['employee_id'], 'expired'=>0])
                            ->get()->row();

           $last_year = $this->db->select('leave_amount')
                              ->get_where("$this->core_Db.leave_calculation", ['employee_id'=>$post['employee_id'], 'year'=>$total->year])
                              ->row();

           if (!is_null($total) && !is_null($last_year)) {
               return $total->balance + ($last_year->leave_amount - $total->balance);
           } else {
               return 0;
           }
       }
   }
