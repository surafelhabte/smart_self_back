<?php
   class Vacancy_Model extends CI_Model
   {
       public function __construct()
       {
           parent::__construct();
           $this->core_Db=config_item('core_db');
           $this->recruitment_Db=config_item('recruitment_db');
       }

    public function Apply($post)
    {
        $this->form_validation->set_rules($this->validation())->set_data($post);
        
        if ($this->form_validation->run()) {
            $result = $this->Validate_pre($post);
            
            if($result['status']){
                $this->db->trans_begin();
                $this->db->insert("$this->recruitment_Db.Application", $post);

                if ($this->db->trans_status() === true) {
                    $this->db->trans_commit();

                    return ['status'=>true,'message'=>'You Sent Application Successfully.'];

                } else {
                    $this->db->trans_rollback();
                    return ['status'=>false,'message'=>['unable to send application.']];
                }

            } else {
                return $result;
            }

        } else {
            return $this->form_validation->error_array();

        }
    }

    public function Withdraw($post)
    {
        $this->form_validation->set_rules($this->validation())->set_data($post);

        if ($this->form_validation->run()) {

            $this->db->trans_begin();
            $this->db->delete("$this->recruitment_Db.Application", $post);

            if ($this->db->trans_status() === true) {
                $this->db->trans_commit();
                return ['status'=>true,'message'=>'You Pull Back from Recruitment Successfully.'];

            } else {
                $this->db->trans_rollback();
                return ['status'=>false,'message'=>'unable to withdraw application.'];
            }

        } else {
            return $this->form_validation->error_array();

        }

    }

    public function Gets()
    {
        $date = date('Y-m-d');
        return $this->db->select("id,Title,Location,Availability,Vacancy_Type,Term_Of_Employement,Dead_line,
                                NumberOfRequired,IF(date(Created_date) = '$date','New',Created_date) as Created_date")
                        ->order_by('Created_date','DESC')
                        ->get("$this->recruitment_Db.Vacancy as vac")
                        ->result_array();
    }

    public function Get($post){
        $result = $this->db->select('vac.id as vac_id,vac.Availability,vac.Location,jo.*,dep.department_name,app.Employee_id')
                        ->from("$this->recruitment_Db.Vacancy as vac")
                        ->where('vac.id',$post['id'])
                        ->join("$this->recruitment_Db.Jobs as jo","vac.JobId = jo.id")
                        ->join("$this->core_Db.department as dep","dep.id = jo.Department")
                        ->join("$this->recruitment_Db.Application as app","app.VacancyId = vac.id AND app.Employee_id = '".$post['employee_id']."'",'Left')
                        ->get()->row();

        $result->qe = $this->db->select('Qualification as qua, Experience as exp')
                                ->get_where("$this->recruitment_Db.Qualification_And_Experience as qe",
                                ['qe.JobId'=>$result->id])->result_array();

        return $result;

    }

    private function validation()
    {
        return [
            ['field' => 'Employee_id','label' => 'Employee_id','rules' => 'required'],
            ['field' => 'VacancyId','label' => 'Vacancy','rules' => 'required'],
        ];
    }

    private function Validate_pre($data){
        $emp = $this->db->select("job_grade as JG,TIMESTAMPDIFF(YEAR,date_of_employment,CURDATE()) as exp,
                                    IFNULL(TIMESTAMPDIFF(YEAR,date_of_employment,es.to_date),0) as on_pos")
                        ->from("$this->core_Db.employee_data as emp")
                        ->where("emp.employee_id = '$data[Employee_id]'")
                        ->join("$this->core_Db.position as pos","emp.position_id = pos.id")
                        ->join("$this->core_Db.employee_status as es","emp.employee_id = es.employee_id","left")
                        ->get()->row();

        $vac_JG = $this->db->select('jo.JobGrade as JG')
                            ->from("$this->recruitment_Db.Vacancy as vac")
                            ->where("vac.id = '$data[VacancyId]'")
                            ->join("$this->recruitment_Db.Jobs as jo","vac.JobId = jo.id")
                            ->get()->row()->JG;

        $rule = $this->db->get("$this->recruitment_Db.Setting")->row();

        if($emp->JG < $vac_JG){
            // process promotion
            if($emp->exp >= $rule->min_year_promotion){
                return $this->Validate_sec($data,$rule,$emp->exp);

            } else {
                return ['status'=> false, 'message'=>["it requires".$rule->min_year_promotion." Service Year In ECX."]]; 
            }

        } elseif ($emp->JG === $vac_JG){
            //process title change & on position
            if($emp->exp >= $rule->min_year_in_ecx){

                if($emp->on_pos >= 2){
                    return $this->Validate_sec($data,$rule,$emp->exp);

                } elseif($emp->on_pos == 0 && $emp->exp >= 5){
                    return $this->Validate_sec($data,$rule,$emp->exp);

                } else {
                    return ['status'=> false, 
                            'message'=>["it requires ".$rule->min_year_on_position."  Service Year on Position."]];
                }

            } else {
                return ['status'=> false, 
                'message'=>["it requires ".$rule->min_year_in_ecx." Service Year in ECX"]]; 
            }

        } else {
            return ['status'=> false, 'message'=>"Downgrade not allowed"]; 
        }
    }

    private function Validate_sec($data, $rule,$exp){
      $total = $this->db->select('(pro_1.value) + (pro_2.value) + (pro_3.value) as total')
                        ->from("$this->recruitment_Db.Sample_APA as Sa")
                        ->where(['Employee_id'=>$data['Employee_id']]) 
                        ->join("$this->recruitment_Db.Assessment_Procedure_Internal as pro_1", "Sa.File = pro_1.type")
                        ->join("$this->recruitment_Db.Assessment_Procedure_Internal as pro_2", "Sa.APA = pro_2.type")
                        ->join("$this->recruitment_Db.Assessment_Procedure_Internal as pro_3",
                            "$exp between pro_3.min_service_year AND pro_3.max_service_year")      
                        ->get()->row()->total;

       $total = 0.25 * $total;                


        if($total >= $rule->min_pass_point){
            //check for education and exprience qualification
            $app_edu = $this->db->select('certificate as cert,subject as sub')
                                ->get_where("$this->core_Db.education",['employee_id'=>$data['Employee_id']])
                                ->result();
                
            $app_exp = $this->db->select('IFNULL(sum(TIMESTAMPDIFF(YEAR,from_date,to_date)),0) as exp')
                                ->get_where("$this->core_Db.experience",['employee_id'=>$data['Employee_id']])->row()->exp;

            // $app_exp = (float)$exp + (float)$app_exp;  
            $app_exp = 0;                                            

            $job_qua = $this->db->select('qe.Qualification as qua, qe.Experience as exp,Field_Of_Study as fs')
                                ->from("$this->recruitment_Db.Vacancy as vac")
                                ->where(['vac.id'=>$data['VacancyId']])
                                ->join("$this->recruitment_Db.Qualification_And_Experience as qe","qe.JobId = vac.JobId") 
                                ->join("$this->recruitment_Db.Jobs as jo","jo.id = vac.JobId") 
                                ->get()->result();

            $field_of_study = json_decode($job_qua[0]->fs, true);                   
                
            if (count(array_intersect(array_column($app_edu, 'sub'), $field_of_study)) > 0) {
                // If the Applicant have the required field of study
                $errors = [];

                foreach($job_qua as $JQ){
                    foreach($app_edu as $AE){

                        if($JQ->qua === $AE->cert){
                            if($JQ->exp <= $app_exp){
                                return ['status' => true, 'message' => 'Qualified'];

                            } else {
                                array_push($errors, 'Job it requires ' . $JQ->exp . ' year for '. $JQ->qua);

                            }

                        } else {
                            array_push($errors, 'Job it requires ' . $JQ->qua);

                        }
                    }
                }
                
                return ['status' => false, 'message' => $errors];

            } else {
                return ['status'=> false,'message'=>["Job it requires the following Field Of Study :- "
                                                    . implode( " , ", $field_of_study)]];
                
            }

        } else {
            return ['status'=> false, 
            'message'=>["Your Total $total % (APA, File Clearance & Exprience)  is below Required Minimum Point $rule->min_pass_point % "]]; 
        }
    }

   }
