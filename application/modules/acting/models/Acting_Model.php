<?php
   class Acting_Model extends CI_Model
   {
       public function __construct()
       {
           parent::__construct();
           $this->load->helper('uuid_gen');
           $this->core_Db=config_item('core_db');
       }
        
       public function create($post)
       {
           $this->form_validation->set_rules(array_merge($this->validation_request(), array(array('field' => 'data','label' => 'data','rules' => 'required'))))
                              ->set_data($post);

      
           if ($this->form_validation->run()) {
               if (request_status($post['action_id'], $post['emp_id'])) {
                   $data = json_decode($post['data'], true);
                   unset($post['data']);
                   $post['id'] = uuid_gen();
  
                   $data['request_id'] = $post['id'];
                   $data['id'] = uuid_gen();
  
                   $this->form_validation->reset_validation()->set_rules($this->validation_body())->set_data($data);
  
                   if ($this->form_validation->run()) {
                       $this->db->trans_begin();
                       $this->db->insert('tbl_request', $post);
                       $this->db->insert('tbl_acting_assignment', $data);
    
                       if ($this->db->trans_status() === true) {
                           $this->db->trans_commit();
                           return ['status'=>true,'message'=>'acting assignment request sent successfully.'];
                       } else {
                           $this->db->trans_rollback();
                           return ['status'=>false,'message'=>'unable to send the request.'];
                       }
                   } else {
                       return $this->form_validation->error_array();
                   }
               } else {
                   return ['status'=>false,'message'=>'You can not send this request at this stage while your previous request is inprocess !'];
               }
           } else {
               return $this->form_validation->error_array();
           }
       }

       public function update($post)
       {
           $this->form_validation->set_rules(array(array('field' => 'data','label' => 'data','rules' => 'required')))->set_data($post);

           if ($this->form_validation->run()) {
               $data = json_decode($post['data'], true);
               $this->form_validation->reset_validation()
                              ->set_rules(array_merge($this->validation_body(), array(array('field' => 'id','label' => 'id','rules' => 'required'))))
                              ->set_data($data);

               if ($this->form_validation->run()) {
                   $this->db->trans_begin();
                   $this->db->update('tbl_acting_assignment', $data, array('id' => $data['id']));
    
                   if ($this->db->trans_status() === true) {
                       $this->db->trans_commit();
                       return ['status'=>true,'message'=>'request updated successfully.'];
                   } else {
                       $this->db->trans_rollback();
                       return ['status'=>false,'message'=>'unable to update request.'];
                   }
               } else {
                   return $this->form_validation->error_array();
               }
           } else {
               return $this->form_validation->error_array();
           }
       }

       public function filter($post)
       {
           $this->form_validation->set_rules($this->validation_request())->set_data($post);

           if ($this->form_validation->run()) {
               return $this->db->select('req.id,req.status,CONCAT(emp.first_name," ", emp.middle_name) As proposed_employee,
                          pos.position,act.starting_date,act.completion_date,req.created_date')
                        ->where('action_id = "'. $post['action_id'] .'" AND req.emp_id = "' . $post['emp_id'] . '"')
                        ->from('tbl_request as req')
                        ->join('tbl_actions As ac', 'ac.id = req.action_id')
                        ->join('tbl_acting_assignment As act', 'act.request_id = req.id')
                        ->join("$this->core_Db.position As pos", 'pos.id = act.position_tobe_filled')
                        ->join("$this->core_Db.employee_data As emp", 'act.proposed_emp_id = emp.employee_id')
                        ->order_by('created_date', 'DESC')
                        ->get()->result_array();
           } else {
               return $this->form_validation->error_array();
           }
       }

       public function get($id)
       {
           return $this->db->select('act.*,req.emp_id,requested_by,
                                CONCAT(emp.first_name," ", emp.middle_name) As proposed_employee, 
                                pos.position As proposed_position,
                                p_pos.position as current_position,pos.job_grade As proposed_job_grade')
                      ->from('tbl_request as req')
                      ->join('tbl_acting_assignment as act', 'act.request_id = req.id')
                      ->join("$this->core_Db.position As pos", 'pos.id = act.position_tobe_filled')
                      ->join("$this->core_Db.employee_data As emp", 'emp.employee_id = act.proposed_emp_id')
                      ->join("$this->core_Db.employee_data As p_emp", 'p_emp.employee_id = act.proposed_emp_id')
                      ->join("$this->core_Db.position As p_pos", 'p_pos.id = p_emp.position_id', 'left')
                      ->where(['req.id'=>$id])
                      ->get()->row();
       }

       public function delete($id)
       {
           if ($this->db->where(['id'=>$id, 'status'=>null])->from('tbl_request')->count_all_results() > 0) {
               $this->db->trans_begin();
               $this->db->delete('tbl_request', ['id' => $id]);
  
               if ($this->db->trans_status() === true) {
                   $this->db->trans_commit();
                   return ['status'=>true, 'message' =>'Request deleted successfully.'];
               } else {
                   $this->db->trans_rollback();
                   return ['status'=>false, 'message' =>'unable to delete request.'];
               }
           } else {
               return ['status'=>false, 'message' =>'the request is archived. you can not delete at this stage.'];
           }
       }

       private function validation_body()
       {
           return array(
                array('field' => 'position_tobe_filled','label' => 'position','rules' => 'required'),
                array('field' => 'reason_for_vacancy','label' => 'reason for vacancy','rules' => 'required'),
                array('field' => 'requesting_division','label' => 'requesting_division','rules' => 'required'),
                array('field' => 'proposed_emp_id','label' => 'proposed emp_id','rules' => 'required'),
                array('field' => 'starting_date','label' => 'starting date','rules' => 'required'),
                array('field' => 'completion_date','label' => 'completion date','rules' => 'required'),
                array('field' => 'replacement_plan','label' => 'replacement plan','rules' => 'required')
            );
       }

       private function validation_request()
       {
           return array(
             array('field' => 'action_id','label' => 'action','rules' => 'required'),
             array('field' => 'emp_id','label' => 'employee id','rules' => 'required'));
       }
   }
