<?php 
   Class Employee_Model extends CI_Model { 
    
      Public function __construct() { 
         parent::__construct(); 
         $this->core_Db=config_item('core_db');
        } 
        
    public function gets($post) {
        return $this->db->select('employee_id,CONCAT(first_name," ",middle_name) As full_name')
                        ->from("$this->core_Db.employee_data")
                        ->or_like('first_name',$post['keyword'])
                        ->or_like('middle_name',$post['keyword'])
                        ->get()->result_array();
    } 
    
    public function get($post) {
        $date = date('Y');

        $result =  $this->db->select('emp.employee_id as emp_id ,CONCAT(emp.first_name," ",emp.middle_name) as full_name, 
                                    pos.position,emp.location as place_of_work, dep.department_name as division')
                            ->from("$this->core_Db.employee_data as emp")
                            ->where(['emp.employee_id' => $post['emp_id']])
                            ->join("$this->core_Db.position as pos", 'pos.id = emp.position_id')
                            ->join("$this->core_Db.department as dep", 'pos.department_id = dep.id')
                            ->get()->row();  
        
        if(!is_null($result)){
            // $leave = Modules::run('leave/Leave/GetLeaveType', $post);
            $result->leave_balance = $this->get_leave_balance($post)[0]['balance'];

        }  
        
        return $result;
    }

    public function getFamilies($post) {
        $date = date('Y');
        $emp_id = $post['emp_id'];
        $family =  $this->db->select('fam.id,full_name,relation_ship,amount As allowed_amount,one_time_payment')
                        ->from("$this->core_Db.family_info As fam")
                        ->group_by('mc.family_id')
                        ->select_sum('mc.amount_covered','Total')
                        ->where(['fam.employee_id'=>$emp_id])
                        ->join("$this->core_Db.medical_insurance As mi", 'mi.id = fam.medical')
                        ->join("$this->core_Db.medical_coverage As mc", "mc.family_id = fam.id AND year(mc.date) <= $date",'left')
                        ->get()->result_array();

      $self =  $this->db->select('emp.employee_id, first_name full_name,"self" as relation_ship,amount As allowed_amount,one_time_payment')
                        ->from("$this->core_Db.employee_data As emp")
                        ->select_sum('mc.amount_covered','Total')
                        ->where(['emp.employee_id'=>$emp_id])
                        ->join("$this->core_Db.medical_insurance As mi", 'mi.id = emp.medical','left')
                        ->join("$this->core_Db.medical_coverage As mc","mc.refereed_employee = emp.employee_id AND mc.family_id is null AND year(mc.date) = $date")
                        ->get()->result_array();

        return array_merge($family,$self);                
                   
    }

    private function get_leave_balance($post)
    {
        $emp_id = $post['emp_id'];
        return $this->db->select('ld.id,leave_name,per_year,if(per_year=0,null,per_year-ifNull(leave_taken,0)) as balance,carry_over,leave_taken', false)
                ->from("$this->core_Db.leave_definition As ld")
                ->join("$this->core_Db.employee_data As ed", "gender=sex or gender is null or gender=''")
                ->join(
                    "(select employee_id,sum(balance) as balance,leave_id from $this->core_Db.leave_calculation 
                        where expired=0 group by employee_id,leave_id) as lc",
                    "ld.id=lc.leave_id and lc.employee_id=ed.employee_id",
                    "left"
                )

                ->join("(select employee_id,ifNull(sum(IFNULL(leave_taken,0)),0) leave_taken,leave_definition_id
                        from $this->core_Db.leave_grant where leave_to >= concat_ws('-',year(current_date),1,1) and leave_to <= concat_ws('-',year(current_date),12,31)
                        group by employee_id,leave_definition_id) as lg", "ld.id=lg.leave_definition_id and ed.employee_id=lg.employee_id where ed.employee_id='$emp_id'", "left")
                ->get()->result_array();
    }
}

 