<?php 
   Class Experience_Model extends CI_Model { 
    
      Public function __construct() { 
         parent::__construct(); 
         $this->load->helper('uuid_gen');
        } 
        
    public function create($post) {
      $this->form_validation->set_rules(array_merge($this->validation_request(),
        array(array('field' => 'data','label' => 'data','rules' => 'required'))))->set_data($post);

      if ($this->form_validation->run()){

        if(request_status($post['action_id'],$post['emp_id'])){
          
          $data = json_decode($post['data'],true);
          unset($post['data']);

          $post['id'] = uuid_gen();
          $data['request_id'] = $post['id'];
          $data['id'] = uuid_gen();
            
          $this->form_validation->reset_validation()->set_rules($this->validation_body())->set_data($data);
  
          if ($this->form_validation->run('experience')){
  
            $this->db->trans_begin();
            $this->db->insert('tbl_request',$post);
            $this->db->insert('tbl_exprience', $data);
    
            if($this->db->trans_status() === TRUE){
                $this->db->trans_commit();
                return ['status'=>true,'message'=>'work experience request sent successfully.'];

            } else {
                $this->db->trans_rollback();
                return ['status'=>false,'message'=>'unable to send the request.'];              
            }
  
          } else {
            return $this->form_validation->error_array();
          }

        } else{
          return ['status'=>false,'message'=>'You can not send this request at this stage while your previous request is inprocess !'];
        
        }

      } else {
          return $this->form_validation->error_array();
      } 
    }

    public function update($post) {
      $this->form_validation->set_rules(array(array('field' => 'data','label' => 'data','rules' => 'required')))->set_data($post);

        if ($this->form_validation->run()){

          $data = json_decode($post['data'],true);
          $this->form_validation->reset_validation()
                                ->set_rules(array_merge($this->validation_body(), array(array('field' => 'id','label' => 'id','rules' => 'required'))))
                                ->set_data($data);

          if ($this->form_validation->run()){ 
              unset($data['requested_by']);
              $this->db->trans_begin();
              $this->db->update('tbl_exprience',$data, ['id'=>$data['id']]);

            if($this->db->trans_status() === TRUE){
              $this->db->trans_commit();
              return ['status'=>true,'message'=>'request updated successfully.'];
            } else {
              $this->db->trans_rollback();
              return ['status'=>false,'message'=>'unable to update request.'];
            }

          } else {
            return $this->form_validation->error_array();
          }

        } else {
            return $this->form_validation->error_array();
        }

    } 

    public function filter($post) {
      $this->form_validation->set_rules($this->validation_request())->set_data($post);

      if ($this->form_validation->run()){
        return $this->db->select('tbl_request.id,name,status,step,created_date')
                        ->from('tbl_request')
                        ->where(array('action_id'=>$post['action_id'],'emp_id'=>$post['emp_id']))
                        ->join('tbl_actions', 'tbl_actions.id = tbl_request.action_id')
                        ->join('tbl_exprience', 'tbl_exprience.request_id = tbl_request.id')
                        ->order_by('created_date', 'DESC')
                        ->get()->result_array();
      } else {
        return $this->form_validation->error_array();
      } 
    }

    public function get($id) {
      return $this->db->select('tbl_exprience.id,reason,step,requested_by')
                      ->from('tbl_request')
                      ->where(array('tbl_request.id'=>$id))
                      ->join('tbl_exprience', 'tbl_exprience.request_id = tbl_request.id')
                      ->get()->row();   
    }

    public function delete($id) {
      if($this->db->where(['id'=>$id, 'status'=>null])->from('tbl_request')->count_all_results() > 0){
        
        $this->db->trans_begin();
        $this->db->delete('tbl_request', array('id' => $id));
  
        if($this->db->trans_status() === TRUE)
        {
          $this->db->trans_commit();
          return ['status'=>true, 'message' =>'Request deleted successfully.'];
        } else {
          $this->db->trans_rollback();
          return ['status'=>false, 'message' =>'unable to delete request.'];
        }
      } else {
        return ['status'=>false, 'message' =>'the request is archived. you can not delete at this stage.'];

      }
    }  
    
    private function validation_body(){
      return array(array('field' => 'reason','label' => 'reason','rules' => 'required'));
    }

    private function validation_request(){
      return array( 
             array('field' => 'action_id','label' => 'action','rules' => 'required'),
             array('field' => 'emp_id','label' => 'employee id','rules' => 'required'));
    }
    
  } 