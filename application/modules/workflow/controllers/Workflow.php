<?php
use Restserver\Libraries\REST_Controller;
use Restserver\Libraries\REST;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Methods: GET, OPTIONS");

class Workflow extends CI_Controller {

    use REST_Controller {
        REST_Controller::__construct as private __resTraitConstruct;
  }

    function __construct()
    {
        parent::__construct();
        $this->__resTraitConstruct();
        $this->load->library('Validate_Token');
        $this->load->model('Workflow_Model');
    }
 
    public function Create_post() {
        $response = $this->validate_token->authenticateToken($this->input->request_headers());
        if($response){
            $result = $this->Workflow_Model->create($this->post());
            $this->response($result, REST::HTTP_OK);
        } else {
            $this->response(['Not authorized'], REST::HTTP_OK);
        }
    }
  
    public function Update_post()
    {
        $post = $this->post();
        $response = $this->validate_token->authenticateToken($this->input->request_headers());
        if($response){
            $result = $this->Workflow_Model->update($post);
            $this->response($result, REST::HTTP_OK);
        } else {
            $this->response(['Not authorized'], REST::HTTP_OK);
        }
    }

    private function Gets_get()
    {
        $response = $this->validate_token->authenticateToken($this->input->request_headers());
        if($response){
            $result = $this->Workflow_Model->gets();
            $this->response($result, REST::HTTP_OK);
        } else {
            $this->response(['Not authorized'], REST::HTTP_OK);
        }
    }


    private function Get_get($id)
    {
        $response = $this->validate_token->authenticateToken($this->input->request_headers());
        if($response){
            $result = $this->Workflow_Model->get($id);
            $this->response($result, REST::HTTP_OK);
        } else {
            $this->response(['Not authorized'], REST::HTTP_OK);
        }
    }

    public function Delete_delete($id) {
        $response = $this->validate_token->authenticateToken($this->input->request_headers());
        if($response){
            $result = $this->Workflow_Model->delete($id);
            $this->response($result, REST::HTTP_OK);
        } else {
            $this->response(['Not authorized'], REST::HTTP_OK);
        }
    }

    public function Remove_delete($id) {
        $response = $this->validate_token->authenticateToken($this->input->request_headers());
        if($response){
            $result = $this->Workflow_Model->remove($id);
            $this->response($result, REST::HTTP_OK);
        } else {
            $this->response(['Not authorized'], REST::HTTP_OK);
        }
    }
}
 