<?php 
   Class Workflow_Model extends CI_Model { 
    
      Public function __construct() { 
         parent::__construct(); 
         $this->load->helper('uuid_gen');
         $this->core_Db=config_item('core_db');
        } 
        
    public function create($post) {
      $this->form_validation->set_rules([
        ['field' => 'workflow','label' => 'workflow','rules' => 'required'],
        ['field' => 'action_id','label' => 'action_id','rules' => 'required']
      ]);

      $post['workflow'] = json_encode($post['workflow'],true);
      $this->form_validation->set_data($post);

      if ($this->form_validation->run() === TRUE){
          $post['workflow'] = json_decode($post['workflow'],true);

          $this->form_validation->reset_validation();
          $this->form_validation->set_rules($this->validation_body());

          $workflow = $post['workflow'];
          unset($post['workflow']);

          foreach($workflow as &$value){
            $value['action_id'] = $post['action_id'];
            $value = array_merge($value, $post);
            $this->form_validation->set_data($value);
            
            if($this->form_validation->run() === False){
              return $this->form_validation->error_array();
            } 

            if($value['authorized_by'] === 0){
              $value['authorized_by'] = null;
            } 

          }

          $this->db->trans_begin();
          $this->db->insert_batch('tbl_activity_workflow', $workflow);
    
          if($this->db->trans_status() === TRUE){
              $this->db->trans_commit();
              return ['status'=>true,'message'=>'activity workflow created successfully.'];
          } else {
              $this->db->trans_rollback();
              return ['status'=>false,'message'=>'unable to create activity workflow.'];
          }

      } else {
          return $this->form_validation->error_array();
      }

    }

    public function update($post) {
      $this->form_validation->set_rules(array(
        array('field' => 'workflow','label' => 'workflow','rules' => 'required'),
        array('field' => 'action_id','label' => 'action_id','rules' => 'required')
      ));

      $post['workflow'] = json_encode($post['workflow'],true);
      $this->form_validation->set_data($post);

      if($this->form_validation->run() === TRUE ){
        $post['workflow'] = json_decode($post['workflow'],true);

        $this->form_validation->reset_validation();
        $this->form_validation->set_rules(array_merge($this->validation_body(), array(array('field' => 'id','label' => 'id','rules' => 'required'))));
        
        $workflow = $post['workflow'];
        unset($post['workflow']);
        $data_without_id = array();

        foreach($workflow as $key => &$value){

          if(!array_key_exists('id',$value)){
            $value = array_merge($value,$post);
            array_push($data_without_id, $value);
            array_splice($workflow,$key,1);
            
          } else{
            $value = array_merge($value, $post);
          }

          $this->form_validation->set_data($value);

          if($this->form_validation->run() === False){
            return $this->form_validation->error_array();
          } 
        }

        $this->db->trans_begin();
        $this->db->update_batch('tbl_activity_workflow',$workflow, 'id');

        if(count($data_without_id) > 0){
          $this->db->insert_batch('tbl_activity_workflow',$data_without_id);
        }

        if($this->db->trans_status() === TRUE){
          $this->db->trans_commit();
          return ['status'=>true,'message'=>'workflow updated successfully.'];

        } else {
            $this->db->trans_rollback();
            return ['status'=>false,'message'=>'unable to update workflow.'];
        }

      } else {
        return $this->form_validation->error_array();
      } 
    } 
   
    public function gets() {
      return $this->db->select('wf.id,action_id,name,step,job_grade_higher,job_grade_lower,authorized_by,
                                IfNull(pos.position, "Line Manager") as position')
                      ->from('tbl_activity_workflow As wf')
                      ->join('tbl_actions As ac', 'ac.id = wf.action_id')
                      ->join("$this->core_Db.position As pos", 'pos.id = wf.authorized_by','left')
                      ->order_by('job_grade_lower','ASC')
                      ->get()->result_array();
    }

    public function get($id) {
      $x =  $this->db->select('job_grade_lower,job_grade_higher,action_id')
                              ->get_where('tbl_activity_workflow', ['id' => $id])->result_array();


      $workflow = $this->db->select('wf.id,step,authorized_by,pos.position')
                          ->from('tbl_activity_workflow As wf')
                          ->where(['wf.id' => $id]) 
                          ->join("$this->core_Db.position As pos", 'pos.id = wf.authorized_by')->get()->result_array(); 
                          
                        
      return array_merge($x[0],['workflow'=>$workflow]);
            
    }

    public function delete($action_id) {
      $this->db->trans_begin();
      $this->db->delete('tbl_activity_workflow', ['action_id' => $action_id]);

      if($this->db->trans_status() === TRUE)
      {
        $this->db->trans_commit();
        return ['status'=>true, 'message' =>'action deleted successfully.'];
      } else {
        $this->db->trans_rollback();
        return ['status'=>false, 'message' =>'unable to delete action.'];
      }
    }  

    public function remove($id) {
      $this->db->trans_begin();
      $this->db->delete('tbl_activity_workflow', array('id' => $id));

      if($this->db->trans_status() === TRUE)
      {
        $this->db->trans_commit();
        return ['status'=>true, 'message' =>'workflow deleted successfully.'];
      } else {
        $this->db->trans_rollback();
        return ['status'=>false, 'message' =>'unable to delete workflow.'];
      }
    }  

    private function validation_body(){
      return array(
              array('field' => 'action_id','label' => 'action','rules' => 'required'),
              array('field' => 'step','label' => 'step','rules' => 'required|numeric'),
              array('field' => 'authorized_by','label' => 'authorized_by','rules' => 'required')
            );
    }
    
  } 