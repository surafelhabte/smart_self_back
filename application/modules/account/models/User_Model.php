<?php
class User_Model extends CI_Model
{
    public $role = [
        'viewActivityWorkflow','createActivityWorkflow','editActivityWorkflow',
        'deleteActivityWorkflow','viewDashboard','viewSalaryAdvance','viewMedical',
        'viewExperience','viewLeave','viewTermination','viewActingAssignment',
        'viewAttendance','requestForOther','viewVacancy'
    ];
    public function __construct()
    {
        parent::__construct();
        $this->core_Db=config_item('core_db');
    }

    public function Login($data)
    {
        $roles = [];
        foreach ($this->role as $role) {
            array_push($roles, (Object)['role'=>$role, 'status'=>"true"]);
        }
        
        $user_name=$data['username'];
        $password= $data['password'];

        $ad=config_item('ad_authentication');

        $authenticated=true;

        $email=$user_name;

        if ($ad) {
            $account = $this->authenticate($user_name, $password);
            $authenticated=$account->auth_status=='Authentication OK' && $account->mail;
            $email = $account->mail;
        }

        if ($authenticated) {
            $full_name='CONCAT(first_name," ",middle_name," ",last_name) as full_name';

            if (!$ad) {
                $this->db->where(['emp.employee_id' => $data['password']]);
            }

            $result = $this->db->select("$full_name,emp.employee_id,pos.id as position_id,position,salary")
                        ->from("$this->core_Db.employee_data As emp")
                        ->where(['ci.email' => $email])
                        ->join("$this->core_Db.contact_info As ci", 'ci.employee_id = emp.employee_id')
                        ->join("$this->core_Db.position As pos", 'emp.position_id = pos.id')
                        ->limit(1)
                        ->get()
                        ->result_array();
            $result=$result[0]??null;
            if ($result) {
                $result['key'] = $this->validate_token->generateToken($data['password']);
                $result['isAuthenticated'] = true;
                $result['roles'] = $roles;
                return (Object)$result;
            } else {
                return 'incorrect username/password';
            }
        } else {
            return 'incorrect username/password';
        }
    }

    public function authenticate($user_name, $password)
    {
        $this->load->library('ad');
        $user=new stdClass();
        $user->username=$user_name;
        $user->password=$password;
        $this->ad->authenticate($user);
        return $user;
    }
}
