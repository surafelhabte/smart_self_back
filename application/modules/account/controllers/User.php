<?php
use Restserver\Libraries\REST_Controller;
use Restserver\Libraries\REST;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Methods: GET, OPTIONS");

class User extends CI_Controller {

    use REST_Controller {
        REST_Controller::__construct as private __resTraitConstruct;
  }

    function __construct()
    {
        parent::__construct();
        $this->__resTraitConstruct();
        $this->load->library('Validate_Token');
        $this->load->model('User_Model');
    }
  
    public function login_post() {
        $result = $this->User_Model->Login($this->post());
        if(is_object($result)) {
           $result = ['status'=>true,'message'=>$result];
        } else {
           $result = ['status'=>false,'message'=>$result];
        }
        $this->response($result, REST::HTTP_OK);
    }
} 
