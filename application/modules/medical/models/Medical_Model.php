<?php

   class Medical_Model extends CI_Model
   {
       public function __construct()
       {
           parent::__construct();
           $this->load->helper('uuid_gen');
           $this->core_Db=config_item('core_db');
           $this->load->library('UploadFiles');
       }
        
       public function create($post)
       {
           $post['data'] = json_encode($post['data'], true);

           $this->form_validation->set_rules(array_merge(
               $this->validation_request(),
               [['field' => 'data','label' => 'data','rules' => 'required']]
           ))->set_data($post);

           if ($this->form_validation->run()) {
               $data = json_decode($post['data'], true);
               unset($post['data']);
               $post['id'] = uuid_gen();
    
             $result = $this->db->where(['refund_for_id' =>$data[0]['refund_for_id']])
                                ->from('tbl_medical_reimbursement as med')
                                ->join('tbl_request as req', 'med.request_id = req.id AND req.status is null AND emp_id ="'.$post['emp_id'].'"')
                                ->count_all_results();
                        
            if ($result === 0) {
                $this->form_validation->reset_validation()->set_rules($this->validation_body());
                $amount = 0;

                foreach ($data as &$value) {
                    $this->form_validation->set_data($value);

                    if ($this->form_validation->run() === true) {
                        $value['id'] = uuid_gen();
                        $value['request_id'] = $post['id'];
                        $amount = $amount + $value['amount'];
                         // process attachement upload
                        $value['attachement'] = $this->uploadfiles->Convert_From_Base64($value['attachement'],$post['id'],$value['id']);
                    } else {
                        return $this->form_validation->error_array();
                    }
                }

                $this->db->trans_begin();
                $this->db->insert('tbl_request', $post);
                $this->db->insert_batch('tbl_medical_reimbursement', $data);

                if ($this->db->trans_status() === true) {
                    $this->db->trans_commit();
                    return ['status'=>true,'message'=>'medical refund request sent successfully.'];

                } else {
                    $this->db->trans_rollback();
                    $this->uploadfiles->deleteFile($post['id']);
                    return ['status'=>false,'message'=>'unable to send medical refund request.'];
                }
            } else {
                return ['status'=>false,'message'=>'You can not send this request at this stage while your previous request is inprocess !'];

            }
        } else {
            return $this->form_validation->error_array();
        }
    }

    public function update($post)
    {
        $post['data'] = json_encode($post['data'], true);
        $this->form_validation->set_rules([['field' => 'data','label' => 'data','rules' => 'required']])->set_data($post);

        if ($this->form_validation->run()) {
            $data = json_decode($post['data'], true);
            $data_without_id = [];
            $amount = 0;

            $this->form_validation->reset_validation()->set_rules($this->validation_body());

            foreach ($data as $key => &$value) {
                $amount = $amount + $value['amount'];
                 
                   if (!array_key_exists('id', $value)) {
                       $value['id'] = uuid_gen();
                       $value['request_id'] = $post['request_id'];
                       array_push($data_without_id, $value);
                       array_splice($data, $key, 1);

                       // process attachement upload
                       $value['attachement'] = $this->uploadfiles->Convert_From_Base64($value['attachement'], $post['request_id'], $value['id']);
                   } else {
                       // process attachement upload
                       $value['attachement'] = $this->uploadfiles->Convert_From_Base64($value['attachement'], $post['request_id'], $value['id']);
                   }

                   $this->form_validation->set_data($value);
                   if ($this->form_validation->run() === false) {
                       return $this->form_validation->error_array();
                   }
               }
        
               $this->db->trans_begin();
               $this->db->update_batch('tbl_medical_reimbursement', $data, 'id');
               count($data_without_id) > 0 ? $this->db->insert_batch('tbl_medical_reimbursement', $data_without_id) : null;
    
               if ($this->db->trans_status() === true) {
                   $this->db->trans_commit();
                   return ['status'=>true,'message'=>'medical refund request updated successfully.'];
               } else {
                   $this->db->trans_rollback();
                   return ['status'=>false,'message'=>'unable to update medical refund request.'];
               }
           } else {
               return $this->form_validation->error_array();
           }
       }

       public function filter($post)
       {
           $this->form_validation->set_rules($this->validation_request())->set_data($post);

           if ($this->form_validation->run()) {
               return $this->db->select('req.id,"Medical Refund Request" as name,IFNULL(status,"Inprocess") as status,
                                        req.created_date,IFNULL(total_paid,0) as total_paid')
                            ->group_by('req.id')
                            ->select_sum('med.amount', 'requested_amount')
                            ->from('tbl_request as req')
                            ->where(['action_id'=>$post['action_id'],'emp_id'=>$post['emp_id']])
                            ->join('tbl_actions as ac', 'ac.id = req.action_id')
                            ->join('tbl_medical_reimbursement as med', 'med.request_id = req.id')
                            ->order_by('req.created_date', 'DESC')
                            ->get()->result_array();
        } else {
            return $this->form_validation->error_array();
        }
    }

    public function get($id)
    {
        $date = date('Y');
        $refund_for_id = $this->db->select('refund_for_id as id')
                                  ->get_where('tbl_medical_reimbursement', ['request_id'=>$id])
                                  ->row()->id;

        if (is_null($refund_for_id)) {
            $request = $this->db->select('req.id,refund_for_id, CONCAT(emp.first_name," ",emp.middle_name) as full_name,
                                        req.emp_id,req.requested_by,step, mi.amount as assigned')
                                ->select_sum('IFNULL(mc.amount_covered,0)','used')
                                ->from('tbl_request as req')
                                ->where(['req.id'=>$id])
                                ->join('tbl_medical_reimbursement as med', 'med.request_id = req.id')
                                ->join("$this->core_Db.employee_data As emp", 'emp.employee_id = req.emp_id')

                                ->join("$this->core_Db.medical_insurance As mi", 'mi.id = emp.medical','left')
                                ->join("$this->core_Db.medical_coverage As mc", "mc.refereed_employee = emp.employee_id 
                                                       AND mc.family_id is null AND year(mc.date) = $date",'left')
                                ->get()->row();
        } else {
            $request = $this->db->select('req.id,refund_for_id, fam.full_name,fam.relation_ship,req.emp_id,req.requested_by,step, mi.amount as assigned')
                                ->select_sum('IFNULL(mc.amount_covered,0)','used')
                                ->from('tbl_request as req')
                                ->where(['req.id'=>$id])
                                ->join('tbl_medical_reimbursement as med', 'med.request_id = req.id')
                                ->join("$this->core_Db.family_info As fam", 'fam.id = med.refund_for_id', 'left')

                                ->join("$this->core_Db.medical_insurance As mi", 'mi.id = fam.medical','left')
                                ->join("$this->core_Db.medical_coverage As mc", "mc.refereed_employee = req.emp_id 
                                                       AND mc.family_id = $refund_for_id AND year(mc.date) = $date",'left')
                                ->get()->row();
           }


           $data = $this->db->select('id, date, particulars, no_of_receipts,amount,IFNULL(total_paid,0) as total_paid,institution_name,institution_address,attachement')
                         ->get_where('tbl_medical_reimbursement as med', ['refund_for_id'=>$request->refund_for_id,'request_id'=>$id])
                         ->result_array();
                    
           return array_merge((array)$request, ['data'=>$data]);
       }
    
       public function getFamilyMembers($post)
       {
           $date = date('Y');
           $family = $this->db->select('fam.id,full_name,relation_ship,amount As allowed_amount,one_time_payment')
                            ->select_sum('mc.amount_covered', 'Total')
                            ->from("$this->core_Db.family_info As fam")
                            ->join("$this->core_Db.medical_insurance As mi", 'mi.id = fam.medical')
                            ->join("$this->core_Db.medical_coverage As mc", "mc.family_id = fam.id AND year(mc.date) = $date", 'left')
                            ->where(['fam.employee_id'=>$post['emp_id']])
                            ->group_by('mc.family_id')
                            ->get()->result_array();

           $self =  $this->db->select('emp.employee_id As id, concat_ws(" ",first_name,middle_name) full_name,
                                "self" as relation_ship,amount As allowed_amount,one_time_payment')
                        ->select_sum('mc.amount_covered', 'Total')
                        ->from("$this->core_Db.employee_data As emp")
                        ->join("$this->core_Db.medical_insurance As mi", 'mi.id = emp.medical')
                        ->join("$this->core_Db.medical_coverage As mc", "mc.refereed_employee = emp.employee_id AND mc.family_id is null AND year(mc.date) = $date", 'left')
                        ->where(['emp.employee_id'=>$post['emp_id']])
                        ->get()->result_array();
        
           return array_merge($self, $family);
       }

       public function delete($id)
       {
           if ($this->db->where(['id'=>$id, 'status'=>null])->from('tbl_request')->count_all_results() > 0) {
               $this->db->trans_begin();
               $this->db->delete('tbl_request', ['id' => $id]);

               if ($this->db->trans_status() === true) {
                   $this->db->trans_commit();
                   $this->uploadfiles->deleteFile($id);
                   return ['status'=>true, 'message' =>'Request deleted successfully.'];
               } else {
                   $this->db->trans_rollback();
                   return ['status'=>false, 'message' =>'unable to delete request.'];
               }
           } else {
               return ['status'=>false, 'message' =>'the request is archived. you can not delete at this stage.'];
           }
       }

       private function validation_body()
       {
           return [
              ['field' => 'date','label' => 'date','rules' => 'required'],
              ['field' => 'particulars','label' => 'particulars','rules' => 'required'],
              ['field' => 'no_of_receipts','label' => 'no_of_receipts','rules' => 'required'],
              ['field' => 'institution_name','label' => 'institution_name','rules' => 'required'],
              ['field' => 'institution_address','label' => 'institution_address','rules' => 'required'],
              ['field' => 'amount','label' => 'amount','rules' => 'required']
          ];
       }

       private function validation_request()
       {
           return [
             ['field' => 'action_id','label' => 'action','rules' => 'required|numeric'],
             ['field' => 'emp_id','label' => 'employee id','rules' => 'required']
            ];
       }

       public function getBalance($post)
       {
           $date = date('Y');
           $emp_id = $post['emp_id'];

           return $this->db->select('amount')
                    ->select_sum('mc.amount_covered', 'total')
                    ->from("$this->core_Db.employee_data As emp")
                    ->where(['emp.employee_id'=>$emp_id])
                    ->join("$this->core_Db.medical_insurance As mi", 'mi.id = emp.medical')
                    ->join("$this->core_Db.medical_coverage As mc", "mc.refereed_employee = emp.employee_id AND mc.family_id is null AND year(mc.date) = $date")
                    ->get()->row();
       }
   }
