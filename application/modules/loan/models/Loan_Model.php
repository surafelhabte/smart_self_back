<?php 
   Class Loan_Model extends CI_Model { 
    
      Public function __construct() { 
         parent::__construct(); 
         $this->load->helper('uuid_gen');
         $this->core_Db=config_item('core_db');
        } 
        
    public function create($post) {
      $post['data'] = json_encode($post['data'],true);

      $this->form_validation->set_rules(array_merge($this->validation_request(), 
                 [['field' => 'data','label' => 'data','rules' => 'required']]))->set_data($post);
      
      if ($this->form_validation->run()) {
        if(request_status($post['action_id'],$post['emp_id'])){
          
          $data = json_decode($post['data'],true);
          unset($post['data']);
  
          $post['id'] = uuid_gen();
          $post['status'] = 'on-hold';
          $data['request_id'] = $post['id'];
  
          $this->form_validation->reset_validation()->set_rules($this->validation_body())->set_data($data);
          
          if ($this->form_validation->run()){
            $rule = $this->getRules($post['emp_id']);
  
            if($rule->allowed_loan_amount < $data['amount']){
              return ['status'=>false,'message'=>'your maximum salary advance is ' . $rule->allowed_loan_amount];
            }
  
            $data['id'] = uuid_gen();
            $data['paid_per_month'] = ($data['amount'] / $rule->loan_repay_period);
            $data['paid_within_month'] =  $rule->loan_repay_period;
  
            $loan_guarantors = $data['guarantor'];
            unset($data['guarantor']);
  
            foreach($loan_guarantors as &$value){
              $value['id'] = uuid_gen();
              $value['loan_id'] = $data['id'];
            }
  
            $this->db->trans_begin();
            $this->db->insert('tbl_request',$post);
            $this->db->insert('tbl_loan', $data);
            $this->db->insert_batch('tbl_loan_guarantors',$loan_guarantors);
    
            if($this->db->trans_status() === TRUE){
                $this->db->trans_commit();
                return ['status'=>true,'message'=>'salary advance request sent successfully.'];
            } else {
                $this->db->trans_rollback();
                return ['status'=>false,'message'=>'unable to send the request.'];
            }
  
          } else {
            return $this->form_validation->error_array();
          }
          
        } else{
          return ['status'=>false,'message'=>'You can not send this request at this stage while your previous request is inprocess !'];
        
        }

      } else {
          return $this->form_validation->error_array();
      } 

    }

    public function update($post) {
      $post['data'] = json_encode($post['data'],true);

      $this->form_validation->set_rules([['field' => 'data','label' => 'data','rules' => 'required']])->set_data($post);
      if ($this->form_validation->run()) {
        $data = json_decode($post['data'],true);

        $this->form_validation->reset_validation()
                              ->set_rules(array_merge($this->validation_body(), [['field' => 'id','label' => 'id','rules' => 'required']]))
                              ->set_data($data);

        if ($this->form_validation->run()){ 
          $rule = $this->getRules($post['emp_id']);

          if($rule->allowed_loan_amount < $data['amount']){
            return ['status'=>false,'message'=>'your maximum salary advance is ' . $rule->allowed_loan_amount];
          }
          $data['paid_per_month'] = ($data['amount'] / $rule->loan_repay_period);
          $data['paid_within_month'] =  $rule->loan_repay_period;

          $guarantors = $data['guarantor'];
          unset($data['guarantor']);

          $data_without_id = [];
          foreach($guarantors as $key => &$value){
            if(!array_key_exists('id',$value)){
              $value['id'] = uuid_gen();
              $value['loan_id'] = $data['id'];
              array_push($data_without_id, $value);
              array_splice($guarantors,$key,1);
            } 
          }

          $this->db->trans_begin();
          $this->db->update('tbl_loan',$data, ['request_id'=>$data['id']]);
          $this->db->update_batch('tbl_loan_guarantors',$guarantors, 'id');

          if(count($data_without_id) > 0){
            $this->db->insert_batch('tbl_loan_guarantors',$data_without_id);
          }
  
          if($this->db->trans_status() === TRUE){
            $this->db->trans_commit();
            return ['status'=>true,'message'=>'salary advance request updated successfully.'];
          } else {
            $this->db->trans_rollback();
            return ['status'=>false,'message'=>'unable to update request.'];
          }
        } else {
          return $this->form_validation->error_array();
        }

      } else {
          return $this->form_validation->error_array();
      }

    } 
   
    public function filter($post) {
      $this->form_validation->set_rules($this->validation_request())->set_data($post);

      if ($this->form_validation->run()){
        return $this->db->select('req.id,status,amount,req.created_date')
                        ->from('tbl_request as req')
                        ->where(['action_id'=>$post['action_id'],'emp_id'=>$post['emp_id']])
                        ->join('tbl_actions as ac', 'ac.id = req.action_id')
                        ->join('tbl_loan lo', 'lo.request_id = req.id')
                        ->order_by('req.created_date', 'DESC')
                        ->get()->result_array();
      } else {
        return $this->form_validation->error_array();
      } 
    }
 
    public function get($id) {
      $loan = $this->db->select('lo.id,amount,reason,paid_per_month,paid_within_month,status,created_date,step,emp_id,requested_by')
                      ->from('tbl_request as req')
                      ->where(['req.id'=>$id])
                      ->join('tbl_loan as lo', 'lo.request_id = req.id')
                      ->get()->result_array();   
      
      $loan[0]['guarantor'] = $this->db->select('gtr.id,gtr.emp_id,CONCAT(emp.first_name," ",emp.middle_name) as full_name')
                                       ->from('tbl_loan_guarantors as gtr')
                                       ->where(['loan_id' => $loan[0]['id']])
                                       ->join("$this->core_Db.employee_data As emp", 'emp.employee_id = gtr.emp_id')
                                       ->get()->result_array();

      return $loan[0];              
    }

    public function delete($id) {
      if($this->db->where("(id = '$id' AND status is null) OR (id = '$id' AND status = 'on-hold')")->from('tbl_request')->count_all_results() > 0){
        
        $this->db->trans_begin();
        $this->db->delete('tbl_request', array('id' => $id));
  
        if($this->db->trans_status() === TRUE)
        {
          $this->db->trans_commit();
          return ['status'=>true, 'message' =>'Request deleted successfully.'];
        } else {
          $this->db->trans_rollback();
          return ['status'=>false, 'message' =>'unable to delete request.'];
        }
      } else {
        return ['status'=>false, 'message' =>'the request is archived. you can not delete at this stage.'];

      }
    }  

    public function remove($id) {
      $this->db->trans_begin();
      $this->db->delete('tbl_loan_guarantors', array('id' => $id));

      if($this->db->trans_status() === TRUE)
      {
        $this->db->trans_commit();
        return ['status'=>true, 'message' =>'guarantor deleted successfully.'];
      } else {
        $this->db->trans_rollback();
        return ['status'=>false, 'message' =>'unable to delete guarantor.'];
      }
    }

    private function validation_body(){
      return array(array('field' => 'amount','label' => 'amount','rules' => 'required'),array('field' => 'reason','label' => 'reason','rules' => 'required'));
    }

    private function validation_request(){
      return array( 
             array('field' => 'action_id','label' => 'action','rules' => 'required'),
             array('field' => 'emp_id','label' => 'employee id','rules' => 'required'));
    }

    private function getRules($emp_id) {
      $emp = $this->db->select('TIMESTAMPDIFF(MONTH,`date_of_employment`, CURDATE()) As date,salary')
                      ->get_where("$this->core_Db.employee_data As emp", ['employee_id'=>$emp_id])
                      ->row();

      $result = $this->db->select('allowed_loan_amount,loan_repay_period')
                      ->get_where('tbl_salary_advance_rules','month_minimum <='. $emp->date . ' AND month_maximum >=' . $emp->date)
                      ->result_array()[0];
 
      $result['allowed_loan_amount'] = $emp->salary * $result['allowed_loan_amount'];

      return (Object)$result;
      
    }

    public function getGuarantors($post) {
      return $this->db->select('employee_id,CONCAT(first_name," ",middle_name) As full_name,salary')
                      ->from("$this->core_Db.employee_data")
                      ->group_start()
                          ->or_like('first_name',$post['keyword'])
                          ->or_like('middle_name',$post['keyword'])
                      ->group_end()
                      ->where_not_in('employee_id',$post['emp_id'])
                      ->get()->result_array();
    }  

    public function updateGuarantee($post) {
      $this->form_validation->set_rules(array(
                array('field' => 'loan_id','label' => 'loan id','rules' => 'required'),
                array('field' => 'emp_id','label' => 'emp id','rules' => 'required')),
                array('field' => 'status','label' => 'status','rules' => 'required'))
            ->set_data($post);

      
      if ($this->form_validation->run()) {

          $this->db->trans_begin();
          $this->db->update('tbl_loan_guarantors',['status'=>$post['status']], ['loan_id'=>$post['loan_id'],'emp_id'=>$post['emp_id']]);

          $result = $this->db->where('loan_id = "'. $post['loan_id'].'" AND status is null OR status = "reject"')
                             ->from('tbl_loan_guarantors')
                             ->count_all_results();

          if($result === 0){
            $id=$post['loan_id'];
            $this->db->where("id in (select request_id from tbl_loan where id='$id')")->update('tbl_request',['status'=>null]);
          }

          if($this->db->trans_status() === TRUE){
            $this->db->trans_commit();
            return ['status'=>true,'message'=>'salary advance guarantee request updated successfully.'];

          } else {
            $this->db->trans_rollback();
            return ['status'=>false,'message'=>'unable to update guarantee request.'];

          }
        
      } else {
          return $this->form_validation->error_array();
      }

    } 

    public function getLoanInfo($post){
      $loan =  $this->db->select_sum('total_amount','total')
                        ->select_sum('deducted','deducted')
                        ->select_max('ending_date')
                        ->get_where("$this->core_Db.apply_deduction",['employee_id'=>$post['emp_id']])
                        ->row();

      $rules = $this->getRules($post['emp_id']);

      if($loan->deducted >= ($loan->total / 2)){
        return ['status'=> true, 'message'=>$rules];

      } else {
        return ['status'=> false, 'message'=>$loan];

      }
    }

    public function getLoanBalance($post){
      $loan =  $this->db->select('total_amount as total,(total_amount - deducted) As remaining,ending_date')
                         ->get_where("$this->core_Db.apply_deduction",['employee_id'=>$post['emp_id']])
                         ->result_array();

      $rules = $this->getRules($post['emp_id']);

       if(count($loan) === 0){
        return ['status'=> true, 'message'=>$rules];

       } else {
        $can = 0;
        foreach($loan as $value){
          $value['remaining'] <= ($value['total'] / 2) ? $can = $can + 1 : null;  
        }

        if($can === count($loan)){
          return ['status'=> true, 'message'=>$rules];

        } else {
          return ['status'=> false, 'message'=>$loan];

        }

       }                  
              

    }

  } 
     