<?php
   class Request_Model extends CI_Model
   {
       public function __construct()
       {
           parent::__construct();
           $this->core_Db=config_item('core_db');
       }
        
       public function get($post)
       {
           return $this->db->select('req.id,ac.name,CONCAT(emp.first_name," ",emp.middle_name) as full_name,
                                pos.position,ac.url,req.emp_id,req.updated_date,pos.job_grade')
                      ->from('tbl_activity_workflow as act')
                      ->join('tbl_request as req', 'req.action_id = act.action_id AND req.step = act.step AND req.status is null')
                      ->join('tbl_actions as ac', 'ac.id = act.action_id')
                      ->join("$this->core_Db.employee_data as emp", 'emp.employee_id = req.emp_id')
                      ->join("$this->core_Db.position as pos", 'pos.id = emp.position_id AND pos.job_grade between act.job_grade_lower and act.job_grade_higher')
                      ->where(['act.authorized_by' => $post['position_id']])
                      ->or_group_start()
                        ->where(['is_department_head'=>1,'authorized_by'=>null])
                      ->group_end()
                      ->or_group_start()
                        ->or_where(['is_department_head'=>1,'authorized_by'=>null,'report_to'=>$post['position_id']])
                      ->group_end()
                      ->order_by('req.updated_date', 'DESC')
                      ->get()->result_array();
       }

       public function update($post)
       {
           $ValidationRule= [
                                ['field' => 'id','label' => 'request id','rules' => 'required'],
                                ['field' => 'status','label' => 'status','rules' => 'required']
                            ];
           $this->form_validation->set_rules($ValidationRule)
                    ->set_data($post);

           if ($this->form_validation->run()) {
               $status = $this->db->select('req.step as current,req.action_id')
                          ->select_max('act.step', 'last')
                          ->from('tbl_request as req')
                          ->where(['req.id' => $post['id']])
                          ->join('tbl_activity_workflow as act', 'act.action_id = req.action_id AND act.step >= req.step')
                          ->get()->row();

               $this->db->trans_begin();

               if ($status->last > $status->current) {
                   $this->db->update('tbl_request', ['step' => $status->current + 1], ['id' => $post['id']]);
               } else {
                   $this->db->update('tbl_request', $post, ['id' => $post['id']]);
                   $this->db->insert('tbl_notifications', ['request_id' => $post['id']]);

                   switch ($status->action_id) {
              case 2:
                $this->approve_loan($post['id']);
              break;
              case 3:
                  $this->approve_medical($post['id']);
                  break;
              case 5:
                $this->approve_leave($post['id']);
                break;
              case 8:
                $this->approve_acting($post['id']);
                break;
            }
               }

               if ($this->db->trans_status() === true) {
                   $this->db->trans_commit();
                   return ['status'=>true,'message'=>'request updated successfully.'];
               } else {
                   $this->db->trans_rollback();
                   return ['status'=>false,'message'=>'unable to update request.'];
               }
           } else {
               return $this->form_validation->error_array();
           }
       }

       private function approve_medical($id)
       {
           $result = $this->db->select('total_paid as amount_covered,refund_for_id as family_id,
                      emp_id as refereed_employee')
                      ->select_sum('amount', 'amount_requested')
                      ->from('tbl_medical_reimbursement as med')
                      ->where(['med.request_id' => $id])
                      ->join('tbl_request as req', 'req.id = med.request_id')
                      ->get()
                      ->row();

           $result->date = date('Y-m-d');

           $this->db->insert("$this->core_Db.medical_coverage", $result);
       }

       private function approve_leave($id)
       {
           $result = $this->db->select('from as leave_from,number_of_days as leave_taken,
                                        DATE_ADD(`from`, INTERVAL `number_of_days` DAY) as leave_to,
                                        type as leave_definition_id,comment as reason,emp_id as employee_id')
                      ->from('tbl_leave as lv')
                      ->where(['lv.request_id' => $id])
                      ->join('tbl_request as req', 'req.id = lv.request_id')->get()->result_array()[0];

           $result['rest_day'] = 0;
           $result['approved_by'] = 0;

           $this->db->insert("$this->core_Db.leave_grant", $result);
       }

       private function approve_loan($id)
       {
           $result = $this->db->select('amount as total_amount,paid_per_month as monthly_payable,
                                      paid_within_month as no_of_month,emp_id as employee_id')
                          ->from('tbl_loan as lo')
                          ->where(['lo.request_id' => $id])
                          ->join('tbl_request as req', 'req.id = lo.request_id')
                          ->get()
                          ->result_array()[0];

           $month = $result['no_of_month'];
      
           $result['deduction_id'] = 1;
           $result['year'] = date('Y');
           $result['month'] = date('m');
           $result['starting_date'] = date('Y-m-d');
           $result['ending_date'] = date('Y-m-d', strtotime(" + $month month"));

           $this->db->insert("$this->core_Db.apply_deduction", $result);
       }

       private function approve_acting($id)
       {
           $result = $this->db->select('position_tobe_filled as position_assigned,proposed_emp_id as employee_id
                                ,starting_date as start_date,completion_date as end_date,department_id')
                      ->from('tbl_acting_assignment as act')
                      ->where(['act.request_id' => $id])
                      ->join('tbl_request as req', 'req.id = act.request_id')
                      ->join("$this->core_Db.position as pos", 'pos.id = act.position_tobe_filled')
                      ->get()->row();

           $this->db->insert("$this->core_Db.acting_assignment", $result);
       }

       public function getGuaranteeRequest($post)
       {
           return $this->db->select('req.id,"Guarantee Request" as name, CONCAT(emp.first_name," ",emp.middle_name) as full_name,pos.position, 
                              "guarantor" as url,req.emp_id,req.updated_date, gtr.status')
                      ->from('tbl_loan_guarantors as gtr')
                      ->where(['gtr.emp_id' => $post['emp_id'],'gtr.status'=>null])
                      ->join('tbl_loan as lo', 'lo.id = gtr.loan_id', false)
                      ->join('tbl_request as req', 'req.id = lo.request_id', false)
                      ->join('tbl_actions as ac', 'ac.id = req.action_id', false)

                      ->join("$this->core_Db.employee_data as emp", 'req.emp_id = emp.employee_id', false)
                      ->join("$this->core_Db.position as pos", 'pos.id = emp.position_id', false)

                      ->order_by('req.updated_date', 'DESC')
                      ->get()->result_array();
       }
   }
