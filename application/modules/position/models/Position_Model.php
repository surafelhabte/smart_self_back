<?php 
   Class Position_Model extends CI_Model { 
    
      Public function __construct() { 
         parent::__construct(); 
         $this->core_Db=config_item('core_db');
        } 
        
    public function gets($position) {
        if(is_null($position)){
            return $this->db->select('id,position')
                        ->from("$this->core_Db.position")
                        ->get()->result_array();  
        } else {
            return $this->db->select('id,position')
                            ->from("$this->core_Db.position")
                            ->or_like('position',$position)
                            ->get()->result_array();               
        }
    }    
  
    public function get($post) {
        $dep_id = $this->db->select('department_id')
                        ->from("$this->core_Db.employee_data As emp")
                        ->where(['employee_id'=>$post['emp_id']])
                        ->join("$this->core_Db.position As pos", 'emp.position_id = pos.id')
                        ->get()->row()->department_id;

        return $this->db->select('position,id')->get_where("$this->core_Db.position",['department_id'=>$dep_id])->result_array();                
    }

  }

