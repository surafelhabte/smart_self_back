<?php 
   Class Action_Model extends CI_Model { 
    
      Public function __construct() { 
         parent::__construct(); 
        } 
        
    public function gets() {
      return $this->db->select('id,name,expanded,enabled,selected,subChild,rolename')
                      ->get_where('tbl_actions', 'id <> 1 and id <> 7')
                      ->result_array();

    }

  } 