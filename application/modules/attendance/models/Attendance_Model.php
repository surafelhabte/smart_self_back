<?php 
   Class Attendance_Model extends CI_Model { 
    
      Public function __construct() { 
         parent::__construct(); 
         $this->core_Db=config_item('core_db');
        } 
        
    public function gets($post) {
      return $this->db->select('scan_date,start_time,start_break,end_break,end_time,attendance_status,attendance_description,total_work_time,working_time,absent_hour')
                      ->get_where("$this->core_Db.identity_scan", 
                            'scan_date between "'.$post['start_date'].'" AND "'.$post['end_date'].'" AND employee_id ="'.$post['emp_id'].'"')->result_array();
    }   
    
    public function getWeekly($post) {
      return $this->db->select('scan_date,morning,afternoon')
                      ->get_where("$this->core_Db.identity_scan", 
                          'scan_date between "'. date('Y-m-d',strtotime("-7 day")).'" AND "'. date('Y-m-d') .'" AND employee_id ="'.$post['emp_id'].'"')->result_array();
    }  

  
  } 