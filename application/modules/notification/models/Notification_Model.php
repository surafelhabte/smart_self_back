<?php
   class Notification_Model extends CI_Model
   {
       public function __construct()
       {
           parent::__construct();
           $this->core_Db=config_item('core_db');
       }
        
       public function update($post)
       {
           $this->form_validation->set_rules([
                      ['field' => 'request_id','label' => 'request id','rules' => 'required'],
                      ['field' => 'seen','label' => 'seen','rules' => 'required|numeric']])->set_data($post);
                      
           if ($this->form_validation->run() === true) {
               $this->db->trans_begin();
               $this->db->update('tbl_notifications', $post, ['request_id' => $post['request_id']]);
  
               if ($this->db->trans_status() === true) {
                   $this->db->trans_commit();
                   return ['status'=>true,'message'=>'notification updated successfully.'];
               } else {
                   $this->db->trans_rollback();
                   return ['status'=>false,'message'=>'unable to update notification.'];
               }
           } else {
               return $this->form_validation->error_array();
           }
       }
    
       public function get($post)
       {
           $emp_id = $post['emp_id'];
            return $this->db->select('req.id,name,req.updated_date,req.created_date,req.status')
                            ->from('tbl_request as req')
                            ->join('tbl_notifications as noti', 'noti.request_id = req.id AND noti.seen = 0')
                            ->join('tbl_actions as ac', 'ac.id = req.action_id')
                            ->where("emp_id = '$emp_id' AND status is not null")
                            ->order_by('req.updated_date', 'DESC')
                            ->get()->result_array();
       }

       public function getAll($post)
       {
           $emp_id = $post['emp_id'];
           return $this->db->select('req.id,name,req.updated_date,req.created_date,req.status')
                      ->from('tbl_request as req')
                      ->where("emp_id = '$emp_id' AND status is not null")
                      ->join('tbl_notifications as noti', 'noti.request_id = req.id')
                      ->join('tbl_actions as ac', 'ac.id = req.action_id')
                      ->order_by('req.updated_date', 'DESC')
                      ->get()->result_array();
       }
   }
