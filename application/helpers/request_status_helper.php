<?php
function request_status($action_id, $emp_id)
{
    $CI =& get_instance();
    $result =  $CI->db->where("action_id = '$action_id' AND emp_id = '$emp_id' AND (status is null OR status = 'on-hold')")
                 ->from('tbl_request')
                 ->count_all_results();
    if ($result > 0) {
        return false;
    } else {
        return true;
    }
}

function pre($data)
{
    echo '<pre>';
    print_r($data);
    echo '</pre>';
}

function exits($data)
{
    pre($data);
    exit();
}
