<?php defined('BASEPATH') OR exit('No direct script access allowed');

class UploadFiles {
    
    public function __construct(){}
    
    public function Convert_From_Base64($img, $path, $name) {
        if (preg_match('/^data:image\/[A-z]{1,4};base64,/', $img)) {
            $folderPath = config_item('medical_receipt_url') . $path;

            if (!is_dir($folderPath)) {
                mkdir($folderPath);
            }

            $image_parts = explode(";base64,", $img);
            $image_type_aux = explode("image/", $image_parts[0]);
            $image_type = $image_type_aux[1];
            $image_base64 = base64_decode($image_parts[1]);
            $file = $folderPath . '/'. $name . '.jpeg';
            file_put_contents($file, $image_base64);
            return $file;
            
        } else {
            return $img;

        }

    }

    public function deleteFile($path)
    {
        $path = config_item('medical_receipt_url') . $path;

        if(file_exists($path)){
            $files = glob($path.'/*'); 
              foreach($files as $file){
                if(is_file($file)){
                  unlink($file);
                }  
              }
           
            if (rmdir($path)) {
                return true;

            } else {
                return false;

            }

        } else {
            return false;
            
        }
    }
  
}