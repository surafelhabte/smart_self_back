<?php

$config = array(
    'acting' => array(
        array('field' => 'position_tobe_filled','label' => 'position','rules' => 'required'),
        array('field' => 'reason_for_vacancy','label' => 'reason for vacancy','rules' => 'required'),
        array('field' => 'requesting_division','label' => 'requesting_division','rules' => 'required'),
        array('field' => 'proposed_emp_id','label' => 'proposed emp_id','rules' => 'required'),
        array('field' => 'starting_date','label' => 'starting date','rules' => 'required'),
        array('field' => 'completion_date','label' => 'completion date','rules' => 'required'),
        array('field' => 'replacement_plan','label' => 'replacement plan','rules' => 'required')
    ),
    'workflow' => array(
        array('field' => 'action_id','label' => 'action','rules' => 'required'),
        array('field' => 'step','label' => 'step','rules' => 'required | numeric'),
        array('field' => 'authorized_by','label' => 'authorized_by','rules' => 'required')
    ),
    'create' => array(
        array('field' => 'action_id','label' => 'action','rules' => 'required'),
        array('field' => 'emp_id','label' => 'employee id','rules' => 'required'),
        array('field' => 'data','label' => 'data','rules' => 'required')
    ),
    'update' => array(
        array('field' => 'data','label' => 'data','rules' => 'required')
    ),
    'medical_expense' => array(
        array('field' => 'date','label' => 'date','rules' => 'required'),
        array('field' => 'particulars','label' => 'particulars','rules' => 'required'),
        array('field' => 'no_of_receipts','label' => 'no_of_receipts','rules' => 'required'),
        array('field' => 'refund_for','label' => 'refund_for','rules' => 'required'),
        array('field' => 'refund_for_id','label' => 'refund_for_id','rules' => 'required'),
        array('field' => 'amount','label' => 'amount','rules' => 'required'),
    ),
    'exprience' => array(
        array('field' => 'reason','label' => 'reason','rules' => 'required')
    ),
    'termination' => array(
        array('field' => 'reason_for_terminate','label' => 'termination reason','rules' => 'required')
    ),
    'filter' => array(
        array('field' => 'action_id','label' => 'action','rules' => 'required'),
        array('field' => 'emp_id','label' => 'employee id','rules' => 'required')
    ),
    'leave' => array(
        array('field' => 'from','label' => 'from','rules' => 'required'),
        array('field' => 'to','label' => 'to','rules' => 'required'),
        array('field' => 'type','label' => 'type','rules' => 'required'),
        array('field' => 'certificate_received','label' => 'certificate received','rules' => 'required'),
        array('field' => 'comment','label' => 'comment','rules' => 'required')
    ),
    'loan' => array(
        array('field' => 'amount','label' => 'amount','rules' => 'required'),
        array('field' => 'reason','label' => 'reason','rules' => 'required'),
        array('field' => 'paid_per_month','label' => 'paid_per_month','rules' => 'required'),
        array('field' => 'paid_within_month','label' => 'paid_within_month','rules' => 'required'),
        array('field' => 'guarantor_emp_id','label' => 'guarantor employee id','rules' => 'required')
    ),
    'update_request' => array(
        array('field' => 'request_id','label' => 'request_id','rules' => 'required'),
        array('field' => 'value','label' => 'value','rules' => 'required')
    ),
);